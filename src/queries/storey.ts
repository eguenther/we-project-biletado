const knexConfig = require('./knex_config');
const knex = require('knex')(knexConfig);

import {
  ValidationError,
  NotFoundError,
} from "../error";

export async function getStoreys(includeDeleted: boolean, building_id: string): Promise<any[]>{
  const req_str = (includeDeleted) ? ['*'] : ['id', 'name', 'building_id'];
  return await knex('storeys')
    .select(req_str)
    .where(function(){
      if(!includeDeleted) this.whereNull('deleted_at');
      if(building_id) this.where('building_id', building_id);
    });
}

export async function getStoreyById(id: string): Promise<any> {
  const result = await knex('storeys')
    .select('*')
    .where('id', id)
    .first();
  if(!result) throw new NotFoundError({ext_msg: `Storey [${id}] not found`});
  return Object.fromEntries(Object.entries(result).filter(([_, v]) => v != null));
}

export async function createStorey(name: string, building_id: string) : Promise<any> {
  let result;
  try {
    result = await knex('storeys')
      .insert({'name': name, 'building_id': building_id})
      .whereExists(function(){
        this.select(1)
          .from('buildings')
          .where('id', building_id)
          .whereNull('deleted_at');
      })
      .returning(['id', 'name', 'building_id']);
  } catch(error){
    throw new ValidationError({ext_msg: `Building [${building_id}] does not exist: ${error.message}`})
  }

  if(!result[0]){
    throw new ValidationError({ext_msg: `Building [${building_id}] does not exist`})
  }
  return result[0];
}

export async function deleteStorey(id: string) : Promise<any> {
  const activeRoom = await knex('rooms')
    .select('*')
    .where('storey_id', id)
    .whereNull('deleted_at')
    .first();
  if(activeRoom) {
    throw new ValidationError({
      ext_msg:
        `Deletion not possible: Storey [${id}] contains active rooms`
    });
  }

  const result =  await knex('storeys')
    .update({deleted_at: knex.fn.now()})
    .where('id', id)
    .whereNull('deleted_at')
    .returning('*');

  if(!result[0]){
    throw new NotFoundError({
      ext_msg: `Storey [${id}] not found or already deleted`
    });
  }
  return;
}

export async function upsertStorey(id: string, name: string, building_id: string, restore_deleted: boolean) : Promise<any> {
  let result;
  try{
    result =  await knex('storeys')
      .insert({ id: id, name: name, building_id: building_id, deleted_at: null })
      .onConflict(['id'])
      .merge()
      .where(function(){
        if(!restore_deleted) { this.whereNull('storeys.deleted_at'); }
      })
      .whereExists(function(){
        this.select(1)
          .from('buildings')
          .where('id', building_id)
          .whereNull('deleted_at');
      })
      .returning(['storeys.id', 'storeys.name', 'storeys.building_id', knex.raw('(xmax = 0) as inserted')]);
  } catch(error){
     throw new ValidationError({ext_msg: `Building [${building_id}] does not exist`})
  }

  if(!result[0]){
    throw new ValidationError({ext_msg: `Storey [${id}] deleted and no restore requested or 'parent' building is deleted.`});
  }
  return result[0];
 }