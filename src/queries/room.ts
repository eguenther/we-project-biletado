const knexConfig = require('./knex_config');
const knex = require('knex')(knexConfig);

import {
  ValidationError,
  NotFoundError,
} from "../error";

export async function getRooms(includeDeleted: boolean, storey_id: string): Promise<any[]>{
  const req_str = (includeDeleted) ? ['*'] : ['id', 'name', 'storey_id'];
  return await knex('rooms')
    .select(req_str)
    .where(function(){
      if(!includeDeleted) this.whereNull('deleted_at');
      if(storey_id) this.where('storey_id', storey_id);
  });
}

export async function getRoomById(id: string): Promise<any> {
  const result = await knex('rooms')
    .select('*')
    .where('id', id)
    .first();
  if(!result) throw new NotFoundError({ext_msg: `Room [${id}] not found`});
  return Object.fromEntries(Object.entries(result).filter(([_, v]) => v != null));
}

export async function createRoom(name: string, storey_id: string) : Promise<any> {
  let result;
  try {
    result = await knex('rooms')
      .insert({'name': name, 'storey_id': storey_id})
      .whereExists(function(){
        this.select(1)
          .from('storeys')
          .where('id', storey_id)
          .whereNull('deleted_at');
      })
      .returning(['id', 'name', 'storey_id']);
  } catch(error){
    throw new ValidationError({ext_msg: `Storey [${storey_id}] does not exist: ${error.message}`})
  }

  if(!result[0]){
    throw new ValidationError({ext_msg: `Storey [${storey_id}] does not exist`})
  }
  return result[0];
}

export async function deleteRoom(id: string) : Promise<any> {
  const result =  await knex('rooms')
    .update({deleted_at: knex.fn.now()})
    .where('id', id)
    .whereNull('deleted_at')
    .returning('*');
  if(result.length < 1) throw new NotFoundError({ ext_msg: `Room [${id}] not found or already deleted`});
  return;
}

export async function upsertRoom(id: string, name: string, storey_id: string, restore_deleted: boolean) : Promise<any> {
  const result =  await knex('rooms')
    .insert({ id: id, name: name, storey_id: storey_id, deleted_at: null })
    .onConflict(['id'])
    .merge()
    .where(function(){
      if(!restore_deleted) { this.whereNull('rooms.deleted_at'); }
    })
    .whereExists(function(){
      this.select(1)
        .from('storeys')
        .where('id', storey_id)
        .whereNull('deleted_at');
    })
    .returning(['id', 'name', 'storey_id', knex.raw('(xmax = 0) as inserted')]);

  if(!result[0]){
    throw new ValidationError({ext_msg: `Room [${id}] deleted and no restore requested or 'parent' storey is deleted.`});
  }
  return result[0];
}