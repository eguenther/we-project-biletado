const knexConfig = require('./knex_config');
const knex = require('knex')(knexConfig);
import { logger } from '../logger';

import {
  ValidationError,
  NotFoundError,
} from "../error";

export async function getBuildings(includeDeleted: boolean): Promise<any[]>{
  const req_str = (includeDeleted) ? ['*'] : ['id', 'name', 'address'];
  return await knex('buildings')
    .select(req_str)
    .where(function(){
      if(!includeDeleted) this.whereNull('deleted_at');
    });
}

export async function getBuildingById(id: string): Promise<any> {
  const result = await knex('buildings')
    .select('*')
    .where('id', id)
    .first();
  if(!result) throw new NotFoundError({ext_msg: `Building [${id}] not found`});
  return Object.fromEntries(Object.entries(result).filter(([_, v]) => v != null));
}

export async function createBuilding(name: string, address: string) : Promise<any> {
  const result = await knex('buildings')
    .insert({'name': name, 'address': address})
    .returning(['id', 'name', 'address']);
  return result[0];
}

export async function deleteBuilding(id: string) : Promise<any> {
  const activeStorey = await knex('storeys')
    .select('*')
    .where('building_id', id)
    .whereNull('deleted_at')
    .first();

  logger.error(activeStorey);
  if(activeStorey) {
    throw new ValidationError({ ext_msg: `Deletion not possible: Building [${id}] contains active storeys` });
  }

  const result =  await knex('buildings')
    .update({deleted_at: knex.fn.now()})
    .where('id', id)
    .whereNull('deleted_at')
    .returning('*');

  logger.error(result);
  if(!result[0]){
    throw new NotFoundError({ ext_msg: `Building [${id}] not found or already deleted.` });
  }
  return;
}

export async function upsertBuilding(id: string, name: string, address: string, restore_deleted: boolean) : Promise<any> {
  const result =  await knex('buildings')
    .insert({ id: id, name: name, address: address, deleted_at: null })
    .onConflict(['id'])
    .merge()
    .where(function(){
      if(!restore_deleted) { this.whereNull('buildings.deleted_at'); }
    })
    .returning(['id', 'name', 'address', knex.raw('(xmax = 0) as inserted')]);

  if(!result[0]){
    throw new ValidationError({ext_msg:`Building [${id}] deleted and no restore requested`});
  }
  return result[0];
 }