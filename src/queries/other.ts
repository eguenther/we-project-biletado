const knexConfig = require('./knex_config');
const knex = require('knex')(knexConfig);

export async function isDbConnected() : Promise<boolean> {
  try {
    await knex.raw("SELECT 1");
    return true;
  } catch(error){
    return false;
  }
}

