import { logger } from "../logger";

module.exports = {
  client: 'pg',
  acquireConnectionTimeout: 10000,
  connection: {
    user: process.env.POSTGRES_ASSETS_USER,
    host: process.env.POSTGRES_ASSETS_HOST,
    database: process.env.POSTGRES_ASSETS_DBNAME,
    password: process.env.POSTGRES_ASSETS_PASSWORD,
    port: process.env.POSTGRES_ASSETS_PORT,
  },
  log: {
    warn(message) { logger.warn(`[KNEX] ${message}`); },
    error(message) { logger.error(`[KNEX] ${message}`); },
    deprecate(message) { logger.silly(`[KNEX] ${message}`); },
    debug(message) { logger.info(`[KNEX] ${message}`); },
  }
};