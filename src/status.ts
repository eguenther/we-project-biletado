import { isDbConnected } from "./queries/other";
import * as rtracer from "cls-rtracer";
import { logger } from "./logger";
import axios from "axios";
import  { ServerError } from "./error";

async function checkDBReady() {
  try {
    return await new Promise<boolean>(async (res, rej) => {
      const timeout = setTimeout(() => rej(new Error('Timeout')), 5000);

      const result = await isDbConnected();
      logger.debug(`DB check result: ${result}`);
      clearTimeout(timeout);

      if (result) {
        logger.debug(`result: ${result}`);
        res(true);
      } else {
        logger.debug(`result: ${result}`);
        res(false);
      }
    });
  } catch (error) {
    logger.error(`Error executing database check: ${error.message}`);
    return false;
  }
}


async function checkKeycloakReady() {
  const host = process.env.KEYCLOAK_HOST;
  const realmName = process.env.KEYCLOAK_REALM;
  const url = `http://${host}/auth/realms/${realmName}`;
  try {
    const response = await axios.get(url);
    logger.debug(response);
    return true;
  } catch (error) {
    logger.error(`Error executing keycloak check: ${error.message}`);
    return false;
  }
}

export function getStatus(req, res) {
  res.status(200).json({
    authors: ["Marcel Fischer", "Erik Günther"],
    api_version: ["2.1.0"],
  });
}

export async function isHealthy(req, res, next){
  const dbStatus = await checkDBReady();
  const keycloakStatus = await checkKeycloakReady();

  if (dbStatus && keycloakStatus) {
    // Beide Status sind in Ordnung
    return res.status(200).json({
      live: true,
      ready: true,
      databases: {
        assets: {
          connected: true,
        },
      },
    });
  } else {
    return next(new ServerError({
      status_code: 'bad_request',
      ext_msg: `[FATAL] Service is not live and should be replaced or restarted!
                Database: ${(dbStatus) ? 'OK' : 'ERR'} | Keycloak: ${(keycloakStatus) ? 'OK' : 'ERR'}`
      })
    );
  }
}

export async function isLive(req, res){
  res.status(200).json({
    live: true,
  });
}

export async function isReady(req, res, next) {
  const dbStatus = await checkDBReady();
  const keycloakStatus = await checkKeycloakReady();

  if (dbStatus && keycloakStatus) {
    // Beide Status sind in Ordnung
    return res.status(200).json({ ready: true });
  } else {
    return next(new ServerError({
        status_code: 'bad_request',
        ext_msg: `[FATAL] Service is not live and cannot serve requests!
                  Database: ${(dbStatus) ? 'OK' : 'ERR'} | Keycloak: ${(keycloakStatus) ? 'OK' : 'ERR'}`
      })
    );
  }
};
