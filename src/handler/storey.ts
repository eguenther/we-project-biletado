import { AssetHandler } from "./asset"
import * as db from "../queries/storey"
import { isValidUUID } from "../utils";
import {
  ValidationError,
  NotFoundError,
} from "../error";

export class StoreyHandler extends AssetHandler {
  protected override async getAssetsSub(req){
    let parent_id = req.query["building_id"];
    if(parent_id && !isValidUUID(parent_id)){
      parent_id = undefined;
    }
    const include_deleted: boolean = ("true" === req.query.include_deleted);
    return await db.getStoreys(include_deleted, parent_id);
  }
  protected override async getAssetByIdSub(id: string){
    const result = await db.getStoreyById(id);
    if(!result) throw new NotFoundError();
    return result;
  }
  protected override async createAssetSub(req){
    const { name, building_id } = req.body;
    if(!isValidUUID(building_id)){
      throw new ValidationError({ext_msg: `Invalid building UUID [${building_id}]`});
    }
    const result = await db.createStorey(name, building_id);
    if(!result){
      throw new ValidationError({
        ext_msg:
        `Could not create storey [${name}] in building [${building_id}], maybe building is deleted or does not exist?`
      });
    }
    return result;
  }
  protected override async updateAssetSub(req){
    const id = req.params.id;
    const { name, building_id, deleted_at } = req.body;
    const restore_deleted: boolean = (null === deleted_at);
    return await db.upsertStorey( id, name, building_id, restore_deleted );
  }
  protected override async deleteAssetSub(id: string){
    return await db.deleteStorey(id);
  }
}