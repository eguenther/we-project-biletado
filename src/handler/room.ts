import { AssetHandler } from "./asset"
import * as db from "../queries/room"
import { isValidUUID } from "../utils";
import {
  ValidationError,
  NotFoundError,
} from "../error";

export class RoomHandler extends AssetHandler {
  protected override async getAssetsSub(req){
    let parent_id = req.query["storey_id"];
    if(parent_id && !isValidUUID(parent_id)){
      parent_id = undefined;
    }
    const include_deleted: boolean = ("true" === req.query.include_deleted);
    return await db.getRooms(include_deleted, parent_id);
  }
  protected override async getAssetByIdSub(id: string){
    const result = await db.getRoomById(id);
    if(!result) throw new NotFoundError();
    return result;
  }
  protected override async createAssetSub(req){
    const { name, storey_id } = req.body;
    if(!isValidUUID(storey_id)){
      throw new ValidationError({ext_msg: `Invalid storey UUID [${storey_id}]`});
    }
    const result = await db.createRoom(name, storey_id);
    if(!result){
      throw new ValidationError({ext_msg: `Could not create room [${name}] in storey [${storey_id}]`});
    }
    return result;
  }
  protected override async updateAssetSub(req){
    const id = req.params.id;
    const { name, storey_id, deleted_at } = req.body;
    const restore_deleted: boolean = (null === deleted_at);
    return await db.upsertRoom( id, name, storey_id, restore_deleted );
  }
  protected override async deleteAssetSub(id: string){
    return await db.deleteRoom(id);
  }
}