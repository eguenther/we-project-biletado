import * as authorization from "../authorization";
import { logger } from "../logger";
import { isValidUUID } from "../utils";
import {
  ValidationError,
  NotFoundError,
  ServerError
} from "../error";

interface LogObject {
  status?: string,
  operation?: string,
  object_type?: string,
  object_id?: string,
  user_id?: string
};


export class AssetHandler {
  protected async getAssetsSub(req) : Promise<any[]> { throw new ServerError({ext_msg: "Not implemented"}); }
  protected async getAssetByIdSub(id: string) : Promise<any> { throw new ServerError({ext_msg: "Not implemented"}); }
  protected async createAssetSub(req) : Promise<any> { throw new ServerError({ext_msg: "Not implemented"}); }
  protected async updateAssetSub(req) : Promise<any> { throw new ServerError({ext_msg: "Not implemented"}); }
  protected async deleteAssetSub(req) : Promise<any> { throw new ServerError({ext_msg: "Not implemented"}); }

  getLogObject(params: LogObject) {
    return {
      status: params.status || undefined,
      operation: params.operation || undefined,
      object_type: params.object_type || this.constructor.name.replace("Handler", ""),
      object_id: params.object_id || "unknown",
      user_id: params.user_id || "unknown"
    };
  }

  async getAssets(req, res, next) {
    try {
      const results = await this.getAssetsSub(req);

      for(let i = 0; i < results.length; i++){
        if(results[i].deleted_at === null){
          delete results[i].deleted_at
        }
      }
      return res.status(200).json(results);
    } catch(error){
      return next(error);
    }
  }

  async getAssetById(req, res, next) {
    try {
      const id = req.params.id;
      if(!isValidUUID(id)) {
        throw new NotFoundError({ext_msg: `[${id}] invalid uuid`});
      }
      const result = await this.getAssetByIdSub(id);
      return res.status(200).json(result);
    } catch (error) {
      // Pass error to error handler
      return next(error);
    }
  }

  async createAsset(req, res, next) {
    try {
      await authorization.isAuthorized(req);
      let log = this.getLogObject({
        status: "BEGIN", operation: req.method, user_id: req.userID
      });
      logger.info(log);
      const result = await this.createAssetSub(req);

      log.status = "SUCCESS";
      log.object_id = result.id;
      logger.info(log);

      return res.status(200).json(result);
    } catch (error) {
      return next(error);
    }
  }

  async updateAsset(req, res, next) {
    try {
      await authorization.isAuthorized(req);

      const id = req.params.id;
      let log = this.getLogObject({
        status: "BEGIN", operation: req.method,
        object_id: id, user_id: req.userID
      });
      logger.info(log);

      if(!isValidUUID(id)) {
        throw new ValidationError({ext_msg: `Invalid uuid [${id}]`});
      }

      let result = await this.updateAssetSub(req);
      log.status = "SUCCESS";
      logger.info(log);

      const inserted: boolean = result.inserted;
      delete result.inserted;

      if(inserted) return res.status(201).json(result);
      return res.status(200).json(result);
    } catch (error) {
      return next(error);
    }
  }

  async deleteAsset(req, res, next) {
    try {
      await authorization.isAuthorized(req);

      const id = req.params.id;

      let log = this.getLogObject({
        status: "BEGIN", operation: req.method,
        user_id: req.userID, object_id: id
      });
      logger.info(log);

      if(!isValidUUID(id)) throw new ValidationError({ext_msg: `Invalid uuid [${id}]`});
      await this.deleteAssetSub(id);
      log.status = "SUCCESS";
      logger.info(log);
      return res.status(200).send(`Asset [${id}] successfully deleted`);
    } catch (error) {
      return next(error);
    }
  }
}