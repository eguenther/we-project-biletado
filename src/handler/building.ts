import { AssetHandler } from "./asset"
import * as db from "../queries/building"
import {
  ValidationError,
  NotFoundError,
} from "../error";

export class BuildingHandler extends AssetHandler {
  protected override async getAssetsSub(req){
    const include_deleted: boolean = ("true" === req.query.include_deleted);
    return await db.getBuildings(include_deleted);
  }
  protected override async getAssetByIdSub(id: string){
    const result = await db.getBuildingById(id);
    if(!result) throw new NotFoundError();
    return result;
  }
  protected override async createAssetSub(req){
    const { name, address } = req.body;
    const result = await db.createBuilding(name, address);
    if(!result){
      throw new ValidationError({ext_msg: `Could not create building [${name}]`});
    }
    return result;
  }
  protected override async updateAssetSub(req){
    const id = req.params.id;
    const { name, address, deleted_at } = req.body;
    const restore_deleted: boolean = (null === deleted_at);
    return await db.upsertBuilding( id, name, address, restore_deleted );
  }
  protected override async deleteAssetSub(id: string){
    return await db.deleteBuilding(id);
  }
}