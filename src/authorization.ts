import * as jwt from "jsonwebtoken";
const axios = require("axios");
import * as rtracer from "cls-rtracer";
import { logger } from "./logger";
import { AuthorizationError, ServerError } from "./error";


const trueRegEx : RegExp = /[Tt][Rr][Uu][Ee]/g;
const AUTHORIZATION_ENABLED: boolean = trueRegEx.test(process.env.JWT_AUTHORIZATION_ENABLED);

export async function isAuthorized(req) : Promise<boolean> {
  // In production environment JWT_AUTHORIZATION_ENABLED should always be enabled!
  if(!AUTHORIZATION_ENABLED) return true;

  const auth_header = req.headers.authorization;
  if(!auth_header){
    throw new AuthorizationError({ext_msg: `No authorization header`});
  }

  const host = process.env.KEYCLOAK_HOST;
  const realmName = process.env.KEYCLOAK_REALM;
  const url = `http://${host}/auth/realms/${realmName}`;

  let response;

  try {
    response = await axios.get(url);
  } catch(error) {
    throw new ServerError({message: error.message});
  }

  const realm = response.data;
  const secretKey = `-----BEGIN PUBLIC KEY-----\n${realm.public_key}\n-----END PUBLIC KEY-----`;

  const reqToken = auth_header.replace(/[Bb]earer /, "");

  try {
    const decoded = jwt.verify(reqToken, secretKey);
    req.userID = decoded.sub;
  } catch (error) {
    throw new AuthorizationError({ext_msg: `Not authorized: ${error.message}`});
  }
  return true;
}
