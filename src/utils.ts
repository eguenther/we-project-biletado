const uuid_pattern = new RegExp(
  "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"
);

export function isValidUUID(uuid: string): boolean {
  return uuid_pattern.test(uuid);
}
