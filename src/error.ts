class BiletadoError extends Error {
  status: number;
  status_code: string;
  ext_msg: string;

  constructor(message, status: number, status_code: string, ext_msg?: string) {
    super(message);
    this.name = this.constructor.name;
    this.status = status;
    this.status_code = status_code;
    this.ext_msg = ext_msg;
  }
}

export class ValidationError extends BiletadoError {
  constructor({status = 400,
               status_code = "bad_request",
               message = "Invalid input",
               ext_msg = "" } = {})
  {
    super(message, status, status_code, ext_msg);
  }
}

export class AuthorizationError extends BiletadoError {
  constructor({status = 401,
               status_code = "not_authorized",
               message = "Not authorized",
               ext_msg = "" } = {})
  {
    super(message, status, status_code, ext_msg);
  }
}

export class NotFoundError extends BiletadoError {
  constructor({status = 404,
               status_code = "bad_request",
               message = "Not found",
               ext_msg = "" } = {})
  {
    super(message, status, status_code, ext_msg);
  }
}

export class ServerError extends BiletadoError {
  constructor({status = 503,
               status_code = "no_need_to_know",
               message = "Internal server error",
               ext_msg = "" } = {})
  {
    super(message, status, status_code, ext_msg);
  }
}

