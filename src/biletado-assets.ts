import express from "express";
import cors from "cors";
import * as rtracer from "cls-rtracer";
import { logger } from "./logger";
import { NotFoundError } from "./error";

import { BuildingHandler } from "./handler/building";
import { StoreyHandler } from "./handler/storey";
import { RoomHandler } from "./handler/room";
import * as status from "./status";

const biletadoAssets = express();
const port = process.env.EXPRESS_PORT;

biletadoAssets.use(rtracer.expressMiddleware());
biletadoAssets.use(cors({ origin: true }));
biletadoAssets.use(express.json());

let handler = new Map();
handler.set("buildings", new BuildingHandler());
handler.set("storeys", new StoreyHandler());
handler.set("rooms", new RoomHandler());

for (let [key, h] of handler) {
  logger.info(`Create asset endpoint for ${key}`);
  biletadoAssets.get(`/api/v2/assets/${key}`, h.getAssets.bind(h));
  biletadoAssets.get(`/api/v2/assets/${key}/:id`, h.getAssetById.bind(h));
  biletadoAssets.post(`/api/v2/assets/${key}`, h.createAsset.bind(h));
  biletadoAssets.put(`/api/v2/assets/${key}/:id`, h.updateAsset.bind(h));
  biletadoAssets.delete(`/api/v2/assets/${key}/:id`, h.deleteAsset.bind(h));
}

/* Status */
biletadoAssets.get("/api/v2/assets/status", status.getStatus);
biletadoAssets.get("/api/v2/assets/health", status.isHealthy);
biletadoAssets.get("/api/v2/assets/health/live", status.isLive);
biletadoAssets.get("/api/v2/assets/health/ready", status.isReady);

/*Listen on Port*/
biletadoAssets.listen(port, () => {
  return logger.info(`Express is listening at http://localhost:${port}`);
});

biletadoAssets.use((req, res, next) => {
  return next(new NotFoundError({ext_msg: "Missing parameter or undefined route"}));
});

const errorHandler = (err, req, res, next) => {
  logger.error(err);

  const error = {
    code: err.status_code,
    message: err.message,
    more_info: err.ext_msg,
  };

  res.status(err.status || 500).json({
    error,
    trace: rtracer.id(),
  });
};

biletadoAssets.use(errorHandler);

process.on("SIGINT", () => {
  process.exit();
}); // CTRL+C
process.on("SIGQUIT", () => {
  process.exit();
}); // Keyboard quit
process.on("SIGTERM", () => {
  process.exit();
}); // `kill` command
