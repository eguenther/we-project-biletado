"use strict";

import winston from "winston";

export const logger = winston.createLogger({
  transports: [new winston.transports.Console()],
  format: winston.format.combine(
    winston.format.timestamp(), // adds a timestamp property
    winston.format.json()
  ),
  level: process.env.LOGLEVEL,
});

/*
USAGE:
  import {logger} from './logger'

  logger.error("Error message");
  logger.warn("Warning message");
  logger.info("Info message");
  logger.verbose("Verbose message");
  logger.debug("Debug message");
  logger.silly("Silly message");

  Get UserID: req.userID
*/
