# We-Project-Biletado - The Kubernauts

## Name
biletado-assets backend - Projectgroup: Kubernauts

## Description
This project delivers an API-backend for the Kubernetes environment of Biletado.

You can find more Information here

https://gitlab.com/biletado

and in the quickstart guide, if you want to implement your own api backend

https://gitlab.com/biletado/quickstart

## Installation

To install the project, first set up an podman machine and kind cluster

### Variant 1: Direct installation of biletado
```shell
kubectl create namespace biletado
kubectl config set-context --current --namespace biletado
kubectl apply -k https://gitlab.com/eguenther/we-project-biletado --prune -l app.kubernetes.io/part-of=biletado -n biletado
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s
```

### Variant 2: Download the yaml first 
- download the kustomization.yaml in this repo
- execute the following commands in the folder of the kustomization.yaml :
```shell
kubectl apply -k . --prune -l app.kubernetes.io/part-of=biletado -n biletado
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s
```

### To reload the pod if a new build is available:
```shell
kubectl rollout restart deployment assets
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s
```

## Environment
The resulting container has the following envrionment variables configured

| Env-Variable 			          | 		Default-Value		   | Description 							   								                                                      |
|---------------------------|---------------------|-------------------------------------------------------------------------------------|
| KEYCLOAK_HOST			          | keycloak            | Hostname of the keycloak-service         								                                   |
| KEYCLOAK_REALM            | biletado            | Realm of the keycloak-service            								                                   |
| POSTGRES_ASSETS_USER      | postgres            | Username to access the postgres database 								                                   |
| POSTGRES_ASSETS_PASSWORD  | postgres            | Password to access the postgres database 								                                   |
| POSTGRES_ASSETS_DBNAME    | assets              | DB name of the postgres assets-database  								                                   |
| POSTGRES_ASSETS_HOST      | postgres            | Hostname of the postgres database                                                   |
| POSTGRES_ASSETS_PORT      | 5432                | Port of the postgres database                                                       |
| RESERVATIONS_API_HOST     | http://reservations | URL to access the reservations API (not imnplemented by this project)               |
| LOGLEVEL                  | info                | Setting for the loglevel of the Container (error, warn, info, verbose, debug, silly |              								|
| EXPRESS_PORT              | 3000                | Running Port for the API-Service                                                    |
| JWT_AUTHORIZATION_ENABLED | true                | This should be set to false for local tests only                               |

## Files overview
The application is split into different typescript files.

| File                | Description                                                                                               |
|---------------------|-----------------------------------------------------------------------------------------------------------|
| biletado-assets.ts  | The main file - hands the API calls to the appropriate functions                                          |
| logger.ts           | Logging functionality                                                                                     |
| status.ts           | Implements the status-request functions                                                                   |
| error.ts            | Handles the different error codes                                                                         |
| authorization.ts    | Handles the Authorization via JWT                                                                         |
| utils.ts            | Helper functions like checking for valid UUID                                                             |
| handler/asset.ts    | Contains AssetHandler class which acts as an interface for the different type of request objects and handles requests in a more generic way |
| handler/building.ts | Contains BuildingHandler Class (subclass of AssetHandler), which extracts necessary parameters from the requests and forwards them to the building query functions         |
| handler/storey.ts   | Contains StoreyHandler Class (subclass of AssetHandler), which extracts necessary parameters from the requests and forwards them to the storey query functions           |
| handler/room.ts     | Contains RoomHandler Class (subclass of AssetHandler), which extracts necessary parameters from the requests and forwards them to the room query functions             |
| query/building.ts   | Contains the building specific db queries. These are executed in a secure way and return a result or throw an exception on failure   |
| query/storey.ts     | Contains the storey specific db queries. These are executed in a secure way and return a result or throw an exception on failure   |
| query/room.ts       | Contains the room specific db queries. These are executed in a secure way and return a result or throw an exception on failure   |
| query/other.ts      | Contains other queries like a simple db connection test                                                   |


## Authors and acknowledgment
By Marcel Fischer and Erik Guenther

## License
Licensed under MIT
