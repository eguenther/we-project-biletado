# Fragebogen zum Programmentwurf Webengineering 2 DHBW Karlsruhe TINF21B3

> Checkboxen (`[ ]`) bei Bedarf mit X befüllen: `[X]`

## Gruppeninformationen

Gruppenname: Kubernauts

Gruppenteilnehmer:

- Marcel Fischer
- Erik Günther

## Quellcode

Links zu den Versionskontrollendpunkten:

- https://gitlab.com/eguenther/we-project-biletado


## Lizenz

SPDX-License-Identifier und Dateinamen im Quellcode:
- assets + kustomization
    - MIT: LICENSE
	- https://gitlab.com/eguenther/we-project-biletado/-/blob/main/LICENSE?ref_type=heads

## Sprache und Framework

| Frage                                 | Antwort                                    |
|---------------------------------------|--------------------------------------------|
| Programmiersprache                    | Typescript                                 |   
| Sprachversion                         | 5.3.3                                      |        
| Version ist aktuell und wird gepflegt | [X]                                        |
| Framework (FW)                        | "express.js"                               |
| FW-Version                            | v4.18.2                                    |    
| FW-Version ist aktuell                | [X]                                        | 
| Website zum FW                        | [expressjs.com](https://expressjs.com/de/) |  
| Prepared statements/ORM               | "knex.js"      										                  |
| ORM Version                           | v3.1.0     											                     |
| ORM Version ist aktuell               | [X]     											                        |
| Website zum ORM                       | 	[knex.js](https://knexjs.org/)										                     |

## Automatisierung

Art der Automatisierung:  
* "GitLab CI" 

## Testautomatisierung

Art der Testautomatisierung: 
- "Containerscan mit im test-stage des CI"

Wie sind die Ergebnisse einzusehen?:
* Ergebnisse des Jobs in der Pipelines
* https://gitlab.com/eguenther/we-project-biletado/-/pipelines

## Authentifizierung
* JWT wird berücksichtigt: [X]
* Signatur wird geprüft: [X]

## Konfiguration und Dokumentation

* Dokumentation existiert in bedarfsgerechtem Umfang: [X]
* Konfigurationsparameter sind sinnvoll gewählt: [X]
* keine hardcoded Zugänge zu angeschlossenenen Systemen (URLs, Passwörter, Datenbanknamen, etc.): [X]
* Umgebungsvariablen und Konfurationsdateien sind gelistet und beschrieben: [X]

## Logging
* Logsystem des Frameworks oder Biliothek wurde genutzt: [X]
* winston
* Logs enthalten alle geforderten Werte: [X]
* LogLevel ist konfigurierbar: [X]